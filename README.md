# README #

## Modo de uso ##
Para crear el archivo jar tienes que escribir en consola "make practica" tras esto, tendras que escribir en consola
"java -jar practica.jar nº_lanzamiento" donde nº_lanzamiento es el número de dardos que quieres lanzar, cuanto más
dardos se lance más preciso será el número pi.

## Diagrama UML ##
![alt text](https://bitbucket.org/programacion1tr/practica21/raw/fd37f29c9d603046f1bb81b65a4b65aa99406809/diagrama%20de%20clases.png)
