package mates;
import java.util.*;

public class Matematicas{
	
	public static double generarNumeroPiIterativo(long pasos){
		double dentro = 0;
		double fuera = 0;
		int num = 0;
		/*Random rand = new Random();
                double x = 2*(rand.nextDouble())-1;
                double y = 2*(rand.nextDouble())-1;
                double w = (x*x/y*y);
		*/
		while (num<= pasos){
			Random rand = new Random();
			double x = 2*(rand.nextDouble())-1;
			double y = 2*(rand.nextDouble())-1;
			double w = Math.sqrt(x*x+y*y);
			if (w<=1){
				dentro ++;
			}
			else{
				fuera ++;
			}
			num ++;
		}
		double q = ((4*dentro)/(dentro + fuera));
		return q;
	}
}
